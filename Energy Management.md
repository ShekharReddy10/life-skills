# Energy Management

### 1. Manage Energy Not Time
#### Question 1:

**What are the activities you do that make you relax - Calm quadrant?**  

Below are some of the activities that make me relax:
* Chit-chatting with friends.
* Listening to music.
* Playing video games or indoor games.

#### Question 2:

**When do you find getting into the Stress quadrant?**  

* When I procrastinate the work and rush to do it all at a time.
* When I work continuously for hours to complete before the deadline.
#### Question 3:

**How do you understand if you are in the Excitement quadrant?**  

* When I get good results.
* When I had done anything which is eventually appreciated by everyone.
### Sleep is your superpower
#### Question 4

**Paraphrase the Sleep is your Superpower video in detail.**  

Sleep before learning makes our mind for new challenges and saves new memories in the brain and helps us not to forget when sleep after learning. Sleeping more will makes the brain 40% more efficient than sleeping less.
Aging is one of the important factors that contribute to memory decline, but sleep deprivation is another factor that largely gets ignored.
If you sleep 4 hours a day then your immunity drops by 70% which leads to many cardiovascular problems and in turn affect the immune system.  
 The speaker lists tips for better sleep are:
 - Regularity - go to bed at the same time and wake up at the same time.
 -  Being in a cool environment- allows our body to sleep better when we experience a reduction in surrounding temperature by 2 degrees Celcius.

#### Question 5

**What are some ideas that you can implement to sleep better?**  

Some of the ideas are listed below for the implementation of sleep better:
-   Being regularly to bed at the same time and getting up at the same time
-   Only using the bed for sleep so that our brain makes an association of our bed with sleep
-   sleeping in a room that is cooler than the surroundings.
-   Having light meals before sleep.
-   Getting exercise daily.

#### Question 6

**Paraphrase the video - Brain Changing Benefits of Exercise.**  

-   Exercise is the most transformative thing for the brain. Exercise changes your brain this is proved by discoveries.
-   The reasons for Exercising are:
    - A single workout increases the level of neurotransmitters like dopamine and serotonin in our body which directly increase focus and attention.
	- Exercise increases the reaction time of your body.
	- Exercise changes brain anatomy analogy and function.
- Exercising a minimum of 30 minutes for 3 to 4 days a week is good enough.
- Exercising is not only to have a happier life today but also will protect your brain from an incurable disease and also it changes the trajectory of your life.

#### Question 7

**What are some steps you can take to exercise more?**  

Some of the steps I can take to exercise more:  
* Finding an exercise partner.
* Making challenges in exercising.
* Watching exercise videos during exercise.
* Using stairs.
* Walking to places that are walkable whenever I go.