## Focus Management

**1. What is Deep Work?**

Deep work is the ability to concentrate deeply on a difficult task for long hours consistently without getting distracted.

**2.Paraphrase all the ideas in the above videos and this one in detail**
-   The optimal duration of deep work and how long you should work without taking breaks. The optimal time for deep work should be 45 minutes to 1 hour.
-  Knowing about deadlines and their positive side. The video tells us that a deadline is good because it gives us a motivational signal. It helps you to do the deep work without having any debate with your brain for breaks.
-   The summary of the deep workbook tells that one should practice deep work in day-to-day life. The video also talks about the strategies one can implement for deep work:-
    -  Schedule breaks.
    -  Make deep work a habit.
    -  Get adequate sleep

**3. How can you implement the principles in your day-to-day life?**
The principles I can implement this in the following ways:  
- Getting good sleep.
- Not getting distractions.
- Being focused.
- Practicing deep work for at least 1 hour per day.
- Making all mentioned ways a habit.

**4.Your key takeaways from the video** 
Key takeaways from videos are:
- Using social media for long hours affects the development of the user.
- Social Media is not a fundamental technology and costs the productivity of people badly.
-   Social Media is designed to harness your attention and keep you distracted for a while.
-   The use of social media affects one's mental health badly.

