## Good Practices for Software Development

### Question 1

**What is your one major takeaway from each one of the 6 sections.**   

The major takeaways from each one of the 6 sections are:
- Making sure to ask questions and seek clarity in the meeting itself.
Implementation is taking longer than usual due to some unexpected issue - Inform relevant team members
- The way we ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database are not connecting, need your help, the build is not working, can you help me out, will not get you the answers you are looking for.
- Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
- Remember others have their work to do as well. Pick and choose your communication medium depending on the situation
- Strongly consider blocking social media sites and apps during work hours. Tools like TimeLimit, Freedom, or any other app can be helpful.

### Question 2

**Which area do you think you need to improve on? What are your ideas to make progress in that area?**

- Implementation taking longer than usual - I will make progress by practicing all the concepts thoroughly and finding the best possible ways when I got stuck.
- I will make a data flow on how data is being flowed between components or modules  etc;