# Grit and Growth Mindset
## 1. Grit
#### Question 1

**Paraphrase (summarize) the video in a few lines.**  
IQ was not the only which makes the future brighter and a reality It was Grit that males work hard to make the future a reality. Grit is a passion and perseverance for long-term goals. Grit is having stamina. Grit is sticking with the future day in, and day out it's not for a week, not for months it's for years. Grit is unrelated or even inversely related to the measure of talent. It's all about commitments. The ability of learning is not fixed it can be changed with your efforts. Having Grit is to make continuous efforts with the best ideas and the strongest institutions to test and measure success.

#### Question 2

**What are your key takeaways from the video to take action on?**  
* IQ is a part of measuring the rate of your learning but Grit makes continuous efforts on learning for years.
* Growth mindset is a great idea for building grit.
* Commit with grit.
* Accepting failure which is a temporary state.

## 2. Introduction to Growth Mindset
#### Question 3

**Paraphrase (summarize) the video in a few lines.**  
This is the video about the growth of mindest which was developed by Standford university professor Carol Dweck. She said that is why some people succeed while equally talented people do not. And over the years she discovered that mindset plays an important role in the process. Mindset is two ways of thinking about the learning process. They are Fixed mindset and Growth Mindset. 
The fixed mindset believes that intelligence and skills are set which you either have or don't. They think you are not in control of your abilities.
The growth mindset believes that intelligence and skills are grown and developed. People who are good at something are good because they built that ability. They think you are in control of your abilities. 
A fixed mindset has a constant rate of learning whereas a growth mindset has a linear curve in learning. A growth mindset is bigger than a study. A growth mindset is a foundation to study or learning.
The key ingredients in a Growth mindset are Effort, Challenges, Mistakes, and Feedback. In different times and different situations you might grow or slip to fixed but when you slip to fixed understand the characteristics and importance of a growth mindset and spot issues with beliefs and focus.  
#### Question 4

**What are your key takeaways from the video to take action on?**  
*   Intelligence and skills should be grown with focus and effort.
*  Try to develop the key characteristics of a Growth mindset like efforts, challenges, mistakes, and feedback.
* Put efforts into the learning process.
* Embrace challenges and work through them.
* Take mistakes as an opportunity to learn.
* Appreciate the feedback and use it.

## 3. Understanding Internal Locus of Control
#### Question 5

**What is the Internal Locus of Control? What is the key point in the video?**  

Internal locus of control is to have a belief that you are in control of your destiny and that there will be no issue with motivation again.  
The key points are:
- The students who think they are smart concentrated on easy puzzles and spent no time on hard ones and when they are asked they said they are not enjoying solving puzzles which indicates low levels of motivation.
- The students who worked hard on these puzzles spent time on all the puzzles focusing the majority of time on the hard ones to get solved. when they are asked they said they enjoyed solving those puzzles. It indicates the increased levels of motivation state internal locus of control.  
It is a concept called locus of control - The degree to which you believe you have control over life. 
- How much hard work you put into something is something that you have complete control over. It is a key for stay motivated.
- External locus of control is a belief that they could not do a task because of factors that are outside of their control and because of that they spent less time.
- So, build an internal locus of control that makes you believe yourself to have complete control over your life.

## 4. How to build a Growth Mindset
#### Question 6

**Paraphrase (summarize) the video in a few lines**  
The speaker  tells about how to build a Growth Mindset with the fundamental rules as follows:
 - Believe in your ability to figure things out and makes things better: If you don't know how to work on a particular task but you have a belief in your ability that you can figure it how to work and begin to work on it. But if you don't have any belief then you won't begin the journey of a task.
 - Questioning your assumptions: We always need to question our assumptions like ideas of what we are capable of. Whatever the negative assumptions come out just question that because they prevent a growth mindset.
 - Develop your life curriculum: Think about your passion or long-term goals and develop a curriculum to achieve them.
These make you stronger and explore new ideas to adapt to every situation that happens with the time change.

During the process of a growth mindset, many challenges or difficulties come into existence. Don't quit those difficulties instead honor the struggles and challenges that make you solve new challenges very easily.

#### Question 7

**What are your key takeaways from the video to take action on?**  
The key takeaways from this video are: Honor the struggles.
* Believing in my ability and figuring out things.
* Taking out negative feelings and ideas by questioning.
* Developing a curriculum as todos
* Whenever new challenges come up then I should figure them out without running away or getting upset by them.

## 5. Mindset - A MountBlue Warrior Reference Manual.
**What are one or more points that you want to take action on from the manual? (Maximum 3)**   
* I will stay with a problem till I complete it. I will not quit the problem.
* I will stay relaxed and focused no matter what happens.
* I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.