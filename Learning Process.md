# Learning Process
## 1. How to Learn Faster with the Feynman Technique   
### What is the Feynman Technique? Paraphrase the video in your own words.    
  
Feynman Technique is a method of learning a new concept or to study on an old concept in deeper and knowing all insights thoroughly to understand easier.  

Feynman was a great scientist who received a Nobel prize in 1965 for his work in Quantum Electrodynamics. Besides, being a scientist, he is a great teacher and a great explainer. He is also known as a Great explainer because he makes complex concepts into simple language.   

Feynman's first principle is that you must not fool yourself and you are the easiest person to fool. The four steps of Feynman's techniques are listed below.  
* Take a piece of paper and write down the concept name at the top.
* Illustrate the concept using simple language and diagrams that you make easily understand.
* Identify the problem areas and go back to sources to review and go deeper into understanding.
*  Pinpoint any complicated terms and change them in a simplified way.  
    
The key is to keep any concept in a simplified manner and in a way that makes understanding others who have less or no knowledge on that particular topic. To know how much a particular concept is understood you just think about how you explain this to a kid, that makes you know where your knowledge is good and shaky. This ultimately tests your knowledge on that subject.  

 ### What are the different ways to implement this technique in your learning process?  
   
There are many ways to implement the Feynman technique in the learning process as listed below.  
* The better way to understand a concept is to take notes and start writing on the concept because writing on notes makes you understand more clearly and precisely. Easy to draw diagrams in your way to get a clear understanding of a concept.
* List down the sub-concepts and start making notes on each sub-concept.
* Using simplified and understandable language throughout the notes.
* Better to draw handwritten diagrams for more understanding.
* Having pep talks or making audio clips or video clips to test knowledge in that subject. It makes it easy to know your highs and lows of knowledge in that concept.  

 ## 2. Learning how to learn TED talk by Barbara Oakley.
  
### Paraphrase the video in detail in your own words.  
Barbara Oakley was a professor in researching neuroscience and cognitive psychology by interacting with top experts in those fields and she found the key to learning effectively. She noted that the brain is very complex but easy to simplify its operation in two ways as mentioned below.  
* Focus mode: In is just keeping attention and focused on something to boom it.
* Diffuse mode: The brain is said to be in a set of relaxed neural states.
  
So, when we are learning we go back and forth between those modes and find if we know we are focusing on something just be focused, and if we find something try to learn new concepts or solve a problem and get stuck there just turn our attention away from that concept or problem and allow the brain to go in diffused mode to their work in the background. She illustrated these in real time as mentioned below.
* Salvador Dali was a great artist where he turned to be in resting on a chair holding keys when he tries to create a new painting. When he allows taking rest then he takes ideas from diffused mode to focused mode where he works and creates new paintings. 
* Thomas Alva Edison also sits in char with ball bearings in his hand in a relaxed way, kind of thinking about the problem when he got stuck. Just falling asleep the ball bearings would fall and he woke up with ideas from diffused mode to focused mode and tries to implement his ideas.  
  
Procrastination is a feeling which makes physical pain in the part of the brain to keep attention away from work. This results to be a failure in work. To overcome this Pomodoro technique is all we need which states to set a timer of 25 minutes to keep focused on that work and take a break to get relaxed for a little time. Practicing both attention and the ability to relax a little bit is also important in the learning process.  
  
Poor working memory is also made more creative because when we can't hold ideas tightly then other ideas are comes in like tensions due to problems diverting off into some other idea. Slow thinking is also not at all problem because only hikers can feel the intensity of concepts and their experiences are deeper, and profound and don't jump to early conclusions.  
  
  The illusion of competence in the learning process is like studying all day without any effective approach. Only exercise helps increase the ability to learn and remember. Tests are very important to test all the time by giving yourself mini-tests and studying at different places. Recalling a concept has to be done by having to look at the page and look away to recall concepts helps to know where we are good at knowledge.  
    
### What are some of the steps that you can take to improve your learning process?
  
Some of the steps that can take to improve the learning process are mentioned below.
* When I work on focus then I have to work with the same enthusiasm and when I feel a new way of sorting out problems or concepts I follow to be relaxed and get new ideas to focus mode and try to implement them.
* Pomodoro technology will improve my learning process when I feel procrastinating by considering some time to keep my attention focused on work and taking a little bit of time to relax.
* Exercising on topics keeps me in increase ability to learn and remember.
Taking the mini-test and recalling by looking away from notes.

## 3. Learn Anything in 20 hours.

### Your key takeaways from the video? Paraphrase your understanding.
Key takeaways are:
* Life is a learning process where you have to learn everything that connects directly or indirectly to the learning process.
* Practicing more turns to learning in a short period.
* The way to practice intelligently is the most effective way of learning.
Learning something new is not intellectual it's emotional.

Learning any new skill for a beginner is 10,000 hours as said by Dr.Erricson and later Malcolm Gladwell redefined that 10,000 hours will take to become an expert-level performance. But only 20 hours is enough to learn any new skill. Performance time has to decrease when we practice for more hours. 20 hours is good enough to focus and practice new skills in any domain. The way of practicing intelligently is the most effective way to learn as listed below.
* Deconstruct the skill: Break the skills into different modules and then break them again into smaller pieces. More breakings in modules help to decide what part of the skill would help to get more insights.
* Learn enough to self-correct: Learning on a day has to be within the limit that you have to recall and correct yourself. Learning without self-correct makes no use.
* Remove practice barriers: Keeping aside all disturbing like phones, social media, TV, etc; helps to concentrate more.
Practice at least 20 hours: Learning and practicing have great importance in gaining new skills and Practicing more hours than you learn helps you to remember more.  

### What are some of the steps that you can while approach a new topic?    
Steps to approach a new topic:
Deconstructing skills into many modules and many parts.
* Making a time frame for every module for learning and practicing.
* Removing all barriers like social media and Phones.
Practicing at least one hour on every topic or sub-topics.
* Learning limited topics on a day helps in more understanding and practicing rather than only learning more topics on a day.
Recalling every sub-topic or topic helps me to know the ups and depths of topic knowledge.  
