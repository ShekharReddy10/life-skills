## Listening and Assertive Communication
###  1.  Active Listening
#### Question 1
#### What are the steps/strategies to do Active Listening?  

The steps/strategies to do Active Listening are:
* Avoid getting distractions by your thoughts.
* Focus on the topic and speaker.
* Try not to interrupt the other person or speaker.
* Let the speaker finish and then respond.
* Use phrases.
* Show that you are listening with body language.
* If appropriate take notes during important conversations.
* Paraphrase what others have said to make sure you are on the same page.  

###  2. Reflective Listening
#### Question 2
#### According to Fisher's model, what are the key points of Reflective Listening?

Reflective Listening is responding by reacting to another's point of view and clarifying it.
Key points of Reflective Listening according to Fisher's model:
* More listening than talking.
* Try to understand what the speaker's feelings or perspective contained.
* Get clarification by restating what the speaker said.
* Respond with acceptance and empathy.
* It is important for the reflective listener to respond to negative feelings because this communicates that the listener accepts the unpleasant side.
### 3. Reflection
#### Question 3

#### What are the obstacles in your listening process?

The obstacles in the listening process are:
* Distraction.
* Stuck at the moment when I didn't understand speaker terms.
* Feeling bored sometimes.
Not listening to the speaker's view.
* Neglecting if the speaker's sayings are known.

#### Question 4

#### What can you do to improve your listening?

- Try to understand the speaker's perspectives.
- Being focused and attention to the speaker's view.
- Maintain a note every time I stuck.
- Reviewing myself if the speaker's topic is known already.

### 4. Types of Communication
#### Question 5

#### When do you switch to a Passive communication style in your day-to-day life?
- During conference meetings.
- During any enquires
- When people consider my feelings.
#### Question 6

#### When do you switch to Aggressive communication styles in your day-to-day life?
- When someone is not in control of judging me.
- When someone criticizes always.
- When I or someone got bullied by others unnecessarily.
#### Question 7

#### When do you switch to Passive Aggressive (sarcasm/ gossiping/ taunts/ silent treatment and others) communication styles in your day-to-day life?
- When I feel uncomfortable with others' behavior.
- When others make unnecessary comments.
- When someone does the same mistake again.  
#### Question 8

#### How can you make your communication assertive? 
I can make my communication assertive in the following ways:
-   Expressing needs clearly and appropriately.
-  Using  “I” Statements.
-   Active Listening- listening without interrupting.
-   Speaking calmly and steadily.
-   Having good eye-contact
-   Relaxed body posture
-   Feeling in control.