# Prevention of Sexual Harassment
### -   What kinds of behavior cause sexual harassment?
Sexual harassment is verbal, visual, or physical conduct of a sexual nature that affects working conditions or creates an uncomfortable work environment. 
There are three forms of sexual harassment behavior:
1) **Verbal**
* Comments about clothing.
*  A person's body, sexual or gender-based jokes.
*  Requesting sexual favors repeatedly
* Sexual threats
* Spreading rumors about a person's personal or sexual life. 
* Using foul and obscene language
2) **Visual**
* Posters, drawings, or pictures. 
* Cartoons.
*  Messages or posts in social apps, or texts.
3) **Physical conduct**
* Sexual assault.
*  Blocking movement. 
* Inappropriate touching such as kissing, hugging, patting, or rubbing.
*  Sexual gesturing.
Continuous staring.

Each of the forms above mentioned harassment is divided into two categories.
* **Quit pro quo**: Quit pro quo means "this for that." 
-- This is the type of activity where an employee is offering promotions or firing or threatening to make employees maintain a sexual relationship. 
*  **Hostile work environment**
-- It occurs when an employee's behavior interferes with the work performance of another and creates an uncomfortable environment. 

### -   What would you do in case you face or witness any incident or repeated incidents of such behavior?
In case I face such behavior then I directly go to them and say it's not good enough to behave like that and I take a positive action that makes not criticize them and promotes a happy working environment.  
In case of any witness, I must say the harasser to stop such behavior on a serious note if I got disturbed or the employee needs me to take action. I try to not complain to higher authorities but if the situation demands I will.
