# **ECMASCRIPT 6 - ES6**

ECMAScript 6 is also known as ECMAScript 2015 which was introduced in 2015. ECMAScript was the second major revision to JavaScript after ECMAScript 5. In this ECMAScript, many new features are introduced that made JavaScript easy to use.  
The new [data types](#data-types) and [data structures](#data-structures) are listed below :  
  * Data Types  
    - [Symbol](#symbol)
- Data Structure
  - [Map](#map)
  - [Set](#set)
  - [Weak Map](#weak-map)
  - [Weak Set](#weak-set)

## **Data Types**
ECMAScript supports primitive data types that are string, number, boolean, null and undefined
In ES6, there is an additional primitive data type for JavaScript. ES6 introduced a new primitive data type `Symbol` for supporting unique values.
### **Symbol**

A JavaScript Symbol is a  data type introduced as a new primitive data type in ES6.   
It is just a unique value or unique hidden identifier that helps no other code can accidentally access.   
Let's say,   
```javascript
const mysml1 = Symbol("cat");   
const mysml2 = Symbol("cat"); 
```
When we try to run this code by comparing mysml1 and mysml2 it shows false.   
Output :   
```
mysml1 === mysml2    
false
```   

It is because the mysml1 and mysml2 have both unique values which are hidden as an optional descriptive. Therefore, it shows false as an output.

The Symbol can be used in many ways as follows:
#### **Using property keys**
Symbols are mainly used as unique property keys – a symbol never clashes with any other property key (symbol or string).   
Eg:   
```javascript
let user = {
    id: 10, 
    name: 'Shekhar',
    city: 'Suryapet',
    age: 22
};
#user.id = 20;
const idsym = Symbol('id');
user[idsym] = 1232; 
```
If a user had provided a user.id with 20 and there is an id value in the user, then it will be overridden. To avoid that situation Symbols are used as the key to the property of the user.

If we execute the user then it Symbol creates an optional description value as shown below.   
Output :
```
user {  
    id: 10, name: 'Shekhar', City:  'Suryapet', age: 22, Symbol(id): 1232       
}
```   
#### **Constants representing concepts**
In ES6, you can use symbols and be sure they are always unique.   
Eg:
 ```javascript
 function getColor(color) {
    switch (color) {
        case Red :
            return 'Hello';
        case Blue :
            return 'Hai';
        default :
            console.log("Ok");
    }
}
            const Red = Symbol('red');
            const Blue = Symbol('blue');
            const yy = 'blue';
```
If Symbol is not provided then if color is given as **yy** then it shows Hai instead of Ok. To overcome these Symbols are provided.   
Output :  
```
getColor(yy) # If Symbols are not provided.   
Hai  
getColor(yy)  
OK  
getColor(Blue)  
Hai
```
    
#### **Type Conversion**
Implicitly conversion of symbols to strings throws exceptions
```javascript 
const sym = Symbol('desc');
const str1 = '' + sym; 
const str2 = `${sym}`; 
```
It shows an error if the type conversion is done implicitly.

This can be overcome by converting explicitly:   
```javascript
const str2 = String(sym); 
const str3 = sym.toString();
```

## **Data Structures**
The following four data structures are newly introduced in ECMAScript 6:    
    1. Map.   
    2. WeakMap.  
    3. Set.  
    4. WeakSet.
   
### **Map**
ECMAScript provides a new data structure called Map, which holds the key-value pairs.   
- The keys of the Map can be arbitrary values.
- Keys and values in a Map object may be primitive or objects.
- It returns the new or empty Map.
- Map() accepts an optional iterable object, whose elements are in key-value pairs.


Syntax
```javascript
var map = new Map([iterable]);
```
#### **Map Methods**
Methods | Description  
:--------|:---------   
Map.prototype.keys() | returns all keys in Map.
Map.prototype.values() | returns all values in Map.  
Map.prototype.delete(key)| It is used to delete an entry.
Map.prototype.has(value)| It checks whether or not the corresponding key is in the Map object.    
Map.prototype.clear() | It removes all keys and value pairs in the Map object.
Map.prototype.entries() | It returns every key-value pair in the insertion order of the Map object.

#### **Map Properties**
- Map.size # returns the number of elements in the Map object.

Code :

```js
var map = new Map([[0, "cat"]]);
map.set(1,"Dog");
var v = map.values();
var k = map.keys();
var e = map.entries();
for(i=0;i<map.size;i++)
{
    console.log(v.next().value+" ");//returns all values in Map object.
}
for(i=0;i<map.size;i++)
{
    console.log(k.next().value+" ");//returns all keys in Map object.
}
for(i=0;i<map.size;i++)
{
    console.log(e.next().value+" ");//returns all entries in insertion order of Map object.
}
console.log(map.has(1));// returns true if value. present in Map Object else returns False.
map.delete(1); // It Delete a key-value pair of key 1.
map.clear();// It clears all the key-value pairs.
console.log(map);
```
Output :  
```
Cat  Dog       
0 1  
0, Cat  1, Dog     
true  
{}
```

### **Weak Map** 
Weak Maps are almost similar to Map except for the keys in the Weak Maps should be objects.
- The values are arbitrary values and keys are objects.
- The key-value pairs are weakly stored.
- Weak Map allows keys only if they are objects.
- In a weak Map, the keys are not enumerable. So, there is no method to get the list of keys.  


Code :
```js
let carweakmap = new WeakMap();
let key1 = {
    id : 1
}
let car1 = {
    make : 'Honda',
    model : '4'
}

let key2 = {
    id : 2
}
let car2 = {
    make : 'Maruti',
    model : '5'
}
carweakmap.set(key1,car1);//adds key-value pair to Weak Map
carweakmap.set(key2,car2);
console.log(carweakmap);
carweakmap.delete(car1);//deletes the key-value pair of provided key.
console.lop(carweakmap);
carweakmap.clear();
console.log(carweakmap);
```
Output :
```
WeakMap {Object {id: 1} => Object  {make : "Honda", 4}, Object {id: 1} => Object {make : "Maruti", 5}}  
WeakMap {Object {id: 1} => Object  {make : "Maruti", 4}}  
{}
```
### **Set**
A set is a data structure that allows you to create a collection of unique values. Sets are collections that deal with single objects or single values.

- It allows us to store unique values. 
- It supports both primitive values and object references.
- Sets are ordered so that the elements in sets are iterated in their insertion order.
- It returns the set object.

Syntax :
```js
var set = new Set(iterable);
```

#### **Set Methods**
Methods | Description  
:--------|:---------   
Set.prototype.values() | returns all values in Set Object.  
Set.prototype.delete(value)| It is used to delete an associated value with the corresponding value.
Set.prototype.has(value)| It checks whether or not the corresponding value is in the Set Object.    
Set.prototype.clear() | It removes all values in Set.
Set.prototype.entries() | It returns all values of an array in the insertion order of the Set Object.
Set.prototype.add(value) | It appends a new element to the given value of the set object.

#### **Set Properties** 
- Set.size	# This property returns the number of values in the set object.

Code : 
```js
let myArray = [11, 22, 24, 55, 24];
let mySet = new Set(myArray);
console.log(mySet);
mySet.add('100') // Set can also have string along with numbers.
mySet.add({a : 1, b  : 2});
console.log(mySet);
mySet.delete(11);
console.log(mySet);
mySet.forEach(function(val)){
console.log(val);// It returns all the values in the Set Object.
}
mySet.has(22);
mySet.clear();//It clears all values in the Set Object.
console.log(mySet);
```
Output :  
```
Set {11, 22, 24, 55}  
Set {11, 22, 24, 55, '100', object { a: 1, b: 2}}  
Set {22, 24, 55, '100', object { a: 1, b: 2}}  
22  
24  
55  
'100'   
object { a :1, b : 2}  
true  
{}
```
#### **Weak Set** 
Weak Sets are almost similar to Set expect the values in the Weak Set should be objects.
- The values are objects.
- The values are weakly stored.
- Weak Set allows values only if they are objects.
- In a weak Set, the values are not enumerable. So, there is no method to get the list of values.  


Code :
```js
let carweakset new WeakSet();
let car1 = {
    make: 'Honda';
    model: 4;
}
let ca2 = {
    make: 'Maruti';
    model: 5;
} 
carweakset.add(car1);
carweakset.add(car2);// adds values in to the WeakSet object.
console.log(carweakset);
carweakset.has(car1);// returns true if a value is in WeakSet Object else returns false.
carweakset.delete(car2);// deletes the value if it is present in WeakSet Object.
console.log(carweakset);
carweakset.clear();//It clears all values in the WeakSet Object.
console.log(carweakset);
```
Output :
```
WeakSet {Object {make : 'Honda', model : 4}, Object {make : 'Maruti', model : 5}}  
true 
WeakSet {Object {make: 'Honda', model: 4}}  
{}
```


## **References** 
- <https://exploringjs.com/es6/ch_overviews.html>  
- <https://www.javatpoint.com/es6-set>    
- <https://www.javatpoint.com/es6-map>   
- <https://www.w3schools.com/js/js_es6.asp#mark_symbol>  
- <https://www.youtube.com/watch?v=ycohYSx5h9w&ab_channel=TraversyMedia>    
- <https://www.youtube.com/watch?v=gWbCgJ6KEgg&list=WL&index=112&t=443s&ab_channel=DailyTuition>  
- <https://www.youtube.com/watch?v=4J5hnOCj69w&t=525s&ab_channel=ColtSteele>  
- <http://es6-features.org/#ObjectPropertyAssignment>
