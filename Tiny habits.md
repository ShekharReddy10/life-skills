## Tiny Habits
### 1. Tiny Habits - BJ Fogg
#### Question 1

**Your takeaways from the video (Minimum 5 points)**  
The takeaways from the video are:
- We have to create any habit that makes us stay fit or calm like doing push-ups after doing something else and celebrating after the completion of our work.
-   Clarify the aspiration.
-  Match with specific behaviors.
-  Find a good prompt.
-  Start tiny.

### 2. Tiny Habits by BJ Fogg - Core Message
#### Question 2

**Your takeaways from the video in as much detail as possible**  
The author discovers a universal formula for human behavior (B = MAP)
1.  M = Motivation
2.  Ability
3.  Prompt
If you are developing a new habit, you will perform the habit if your motivation matches the ability required to read the moment you receive a prompt to read. This whole video is divided into three parts.

**Part-1 Shrink the Behaviour**
The author provided a chart to explain the relationship between motivation and ability: if a task is hard, then you need high levels of motivation to rise above the action line and complete the task and in the same way, if the task is easy, you need a little motivation to rise above action line and complete the task. Brushing your teeth is easy to do It doesn’t matter if you're exhausted when you get the prompt to do it; you do it anyway.

**Part-2 Identify an Action Promo**
Three types of habits prompt
1.  External: cues from your environment like post‐it notes, phone notifications, and alarms.
2.  Internal: thoughts and sensations that remind you to act, like a grumbling stomach.
3.  Action prompts: the completion of one behavior reminds you to start the next behavior. Loading the dishwasher can be a prompt to clean the kitchen countertops.

**Part-3 Grow Your Behavior with Some Shine.**
According to the Speaker:
 - You know this feeling already
 -  You feel Shine when you ace an exam. 
 - You feel shone when you give a great presentation and people clap at the end.
 -  You feel Shine when you smell something delicious that you cooked for the first time. When you feel successful at something, even if it’s tiny, your confidence grows quickly, and your motivation increases to do that habit again and perform related behaviors. I call this success momentum. Surprisingly enough, this gets created by the frequency of your successes, not by the size...You that can resist learning to celebrate (small completions), but be aware that you’re choosing not to be as good as you could be at creating habits. For most people, the effort of learning to celebrate is a small price to pay for becoming a Habit Ninja.

#### Question 3

**How can you use B = MAP to make making new habits easier?**  

I can use by following ways:
-   Get tools and resources that help me with the new habit.
-   When we are going for any big task we can use this rule that makes us more comfortable with any kind of condition.
#### Question 4

**Why it is important to "Shine" or Celebrate after each successful completion of a habit?**  
- we need to celebrate after the successful completion of a habit because Celebration is a thing that gives us relief and we remember a lot what kind of situation came in front of us and how we tackle that.

### 3. 1% Better Every Day Video
#### Question 5

**Your takeaways from the video (Minimum 5 points)**
The takeaways from the video are:
-   Habits are compounded, their effect is not that much in the starting but after some time we see a major improvement in ourselves.
-   If you want better results don't just set goals, instead focus on the process of achieving the goal.
-   The most effective way to change your habits is to focus not on what you want to achieve, but on how to achieve it.
-   To make a good habit make it obvious, make it attractive, make it easy, and make it satisfying.
-   Success is the result of daily habits - not one-time life changes. Deciding on the day you are motivated does not make any change because the next day when you are not enough motivated you may forget about it.
-   Getting 1 percent better doesn't mean any numeric 1 percent but getting better by and small change every day.

### 4. Book Summary of Atomic Habits
#### Question 6

**Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes?**

Reading the book makes people concentrate on points they did not distract easily.

#### Question 7

**Write about the book's perspective on how to make a good habit easier?**
Sometimes, people, are more addicted to books, and they lose most of their time there.
#### Question 8

**Write about the book's perspective on making a bad habit more difficult?**  

To make a good habit easier we need to make it:
-  Obvious
-  Attractive
-  Easy
-  Satisfying
To make good habit easier we have to put fewer steps between us and good behaviors.
- Make habits attractive.
- Making it easy means reducing friction and priming our environment for the habits that we would like to develop. Reducing the friction makes it far more likely to do the thing.
- We should try to attach some form of immediate gratification so that we can make habits immediately satisfying.

### 5. Reflection:

#### Question 9:

**Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

I want to complete all my deadlines in dedicated time efficiently. So I want to make it a habit. I will take a piece of paper or use word doc and make a list of our daily habits. One of the ways to change habits is to use a technique called pointing and calling. To make it a habit I will assign a particular time slot when I am going to do it. I will make things interesting so that I can not procrastinate on that work. I will find easy ways to do that work. Finally, I will reward my work for the day.
I follow the rule RRR:
-   Reminder. This is a trigger, or cue, that could be a conscious behavior
-   Routine. This is the behavior associated with the trigger. Doing something over and over can make the behavior routine.
-   Reward. The reward associated with behavior also helps make a habit stick. If we do something that causes enjoyment or relieves distress, the pleasurable release of dopamine in our brain can make us want to do it again.

#### Question 10:

**Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?** 
- Use my phone for entertainment purposes more and I can get eliminate or use it less by making myself some work and performing good habits so I will seek that out.